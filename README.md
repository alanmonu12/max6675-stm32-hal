# MAX6675

Library for MAX6675 sensor with STM32 HAL

# USAGE

``` c
  /* A MAX6675 variable pointer*/
  MAX6675_Typedef* MAX6675;

  /* Create the object*/
  MAX6675 = MAX6675_Create();

  /* Functions */
  MAX6675->MAX6675_getTemp(MAX6675);
  MAX6675->MAX6675_getState(MAX6675);
  MAX6675->MAX6675_getValue(MAX6675);

  HAL_UART_Transmit(&huart2, (uint8_t*)MAX6675->Temp_C, sizeof MAX6675->Value, 1000);

```

# License MIT

Copyright (c) 2019 - Alan Rodriguez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
